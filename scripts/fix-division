#!/usr/bin/env python3
# vim: set ts=8 sts=4 et sw=4 tw=99:
#
# Sometimes divisions look like '198 Open Men RAW'
# This changes that to 'Open Men'.
#
# Overwrites the file in-place if it looks like no information is lost.

import os
import sys
import oplcsv


def fix_division(filename):
    csv = oplcsv.Csv(filename)

    try:
        divindex = csv.index('Division')
        equipindex = csv.index('Equipment')
    except IndexError:
        return

    changed = False

    for row in csv.rows:
        div = row[divindex]

        if div == 'Open Men':
            div = 'Open'
        elif div == 'Open Women':
            div = 'Open'

        if div.endswith(' Men'):
            div = div.replace(' Men', '')
        elif div.endswith(' Women'):
            div = div.replace(' Women', '')

        div = div.replace(' Men ', ' ')
        div = div.replace(' Women ', ' ')

        if div == 'Submaster' or div == 'SubMaster':
            div = 'Submaster 35-39'
        if div == 'Master 35-39':
            div = 'Submaster 35-39'

        div = div.replace('Masters', 'Master')

        # Uh, yeah. USPA sure is standardized.
        div = div.replace("Classic", '')
        div = div.replace("Single-ply", '')
        div = div.replace("Single-Ply", '')
        div = div.replace("Single Ply", '')
        div = div.replace("Singleply", '')
        div = div.replace("SinglePly", '')
        div = div.replace("single Ply", '')
        div = div.replace("Single ply", '')
        div = div.replace("Multi-ply", '')
        div = div.replace("Multi-Ply", '')
        div = div.replace("Multi Ply", '')
        div = div.replace("Multiply", '')
        div = div.replace("MultiPly", '')
        div = div.replace("Multi", '')
        div = div.replace("MULTI Ply", '')
        div = div.replace("Raw", '')
        div = div.replace("RAW", '')
        div = div.replace("raw", '')

        if row[divindex] != div and equipindex == -1:
            if row[equipindex] == '':
                print("Need equipment information")
                sys.exit(1)

        if row[divindex] != div:
            row[divindex] = div
            changed = True

    if changed:
        csv.write_filename(filename)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        fix_division(sys.argv[1])
    else:
        for dirname, subdirs, files in os.walk(os.getcwd()):
            if "meet-data" in subdirs:
                subdirs[:] = ['meet-data']
            if 'entries.csv' in files:
                filepath = dirname + os.sep + 'entries.csv'
                fix_division(filepath)
